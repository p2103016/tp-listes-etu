#include "liste.hpp"
#include "cellule.hpp"

#include <iostream>
#include <cassert>

Liste::Liste() {
  this->cell = nullptr;
}


void delete_cell(Cellule* cell) {
  if (cell == nullptr) {
    return;
  }
  delete_cell(cell->suivante);
  delete cell;
}

Cellule* copy_cell(Cellule* cell) {
  if (cell == nullptr) return nullptr;
  else {
    Cellule* tmp = new Cellule(cell->valeur);
    tmp->suivante = copy_cell(cell->suivante);
    return tmp;
  }
}

Liste::Liste(const Liste& autre) {
  cell = nullptr;
  cell = copy_cell(autre.cell);
}

Liste& Liste::operator=(const Liste& autre) {
  delete_cell(cell);
  cell = copy_cell(autre.cell);
  return *this;
}

Liste::~Liste() {
  delete_cell(cell);
}

void Liste::ajouter_en_tete(int valeur) {
  this->cell = new Cellule(valeur, this->cell);
}

void Liste::ajouter_en_queue(int valeur) {
  if (cell == nullptr) {
    cell = new Cellule(valeur);
    return;
  }
  Cellule* current_cell = cell;
  while (current_cell->suivante != nullptr) {
    current_cell = current_cell->suivante;
  }
  current_cell->suivante = new Cellule(valeur);
}

void Liste::supprimer_en_tete() {
  assert(cell != nullptr);
  Cellule* tmp = cell;
  cell = cell->suivante;
  delete tmp;
}

Cellule* Liste::tete() {
  return cell ;
}

const Cellule* Liste::tete() const {
  return cell ;
}

Cellule* Liste::queue() {
  if (cell == nullptr) return nullptr;
  Cellule* current_cell = cell;
  while (current_cell->suivante != nullptr) {
    current_cell = current_cell->suivante;
  }
  return current_cell;
}

const Cellule* Liste::queue() const {
  if (cell == nullptr) return nullptr;
  Cellule* current_cell = cell;
  while (current_cell->suivante != nullptr) {
    current_cell = current_cell->suivante;
  }
  return current_cell;
}

int Liste::taille() const {
  Cellule* current_cell = cell;
  int taille = 0;
  while (current_cell != nullptr) {
    current_cell = current_cell->suivante;
    taille++;
  }
  return taille ;
}

Cellule* Liste::recherche(int valeur) {
  Cellule* current_cell = cell;
  while (current_cell != nullptr) {
    if (current_cell->valeur == valeur) return current_cell;
    current_cell = current_cell->suivante;
  }
  return nullptr ;
}

const Cellule* Liste::recherche(int valeur) const {
  Cellule* current_cell = cell;
  while (current_cell != nullptr) {
    if (current_cell->valeur == valeur) return current_cell;
    current_cell = current_cell->suivante;
  }
  return nullptr ;
}

void Liste::afficher() const {
  std::string s = "[ ";
  Cellule* current_cell = this->cell;
  while (current_cell != nullptr) {
    s += std::to_string(current_cell->valeur) + " ";
    current_cell = current_cell->suivante;
  }
  std::cout<<s<<"]"<<std::endl;
}
