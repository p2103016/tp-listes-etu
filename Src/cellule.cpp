#include "cellule.hpp"

Cellule::Cellule() {
    this->valeur = 0;
    this->suivante = nullptr;
  }

Cellule::Cellule(int elem, Cellule* suivante) {
    this->valeur = elem;
    this->suivante = suivante;
  }

Cellule::Cellule(int elem) {
    this->valeur = elem;
    this->suivante = nullptr;
  }
