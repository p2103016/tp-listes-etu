#ifndef LIFAP6_LISTE_CELLULE_HPP
#define LIFAP6_LISTE_CELLULE_HPP

class Cellule {


  public :
  Cellule();

  Cellule(int elem);

  Cellule(int elem, Cellule* suivante);

  int valeur;
  Cellule* suivante;

} ;

#endif
